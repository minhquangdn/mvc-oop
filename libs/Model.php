<?php 
	/**
	* Model
	*/
	class Model
	{
		private $hostname = DB_SERVER;
		private $username = DB_USERNAME;
		private $password = DB_PASSWORD;
		private $dbname = DB_DATABASE;

		protected $connect;
		private $flagConnect = true;
		protected $result;

		// Ket noi database
		function __construct()
		{
			$conn = new mysqli($this->hostname, $this->username, $this->password, $this->dbname);
			if($conn->connect_error) {
				die("Can not connect to database: ".$conn->connect_error);
				$this->flagConnect = false;
			}
			else {
				$this->connect = $conn;
			}
		}

		// Thuc thi cau lenh sql
		public function execute($sql) {
			if($this->flagConnect == true) {
				$this->connect->query("SET NAMES 'UTF_8'");
				$this->connect->query("SET CHARACTER SET 'utf-8'");
				$this->result = $this->connect->query($sql);

				return $this->result;
			}
		}

		// Truy xuat du lieu trong bang
		public function fetchAll($table) {
			if($this->flagConnect == true) {
				$sql = "SELECT * FROM ".$table;
				$this->connect->query("SET NAMES 'UTF-8'");
				$this->connect->query("SET CHARACTER SET 'utf-8'");
				$this->result = $this->connect->query($sql);
				return $this->result;
			}
		}
	}
?>