<?php 
	/**
	* Controller
	*/
	
	class Controller
	{
		public function __construct() 
		{
			
		}

		public function load($controller, $action) 
		{
			$controller = ucfirst(strtolower($controller)).'Controller';
			$action = strtolower($action).'Action';

			if(!file_exists('application/controllers/'.$controller.'.php'))
			{
				die('Controller is not exist!');
			}

			$controllerObject = new $controller;
			if(!method_exists($controllerObject, $action))
			{
				die('Action is not exist!');
			}
			$controllerObject->{$action}();

		}
	}

?>