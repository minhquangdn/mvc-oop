<?php 
	/**
	* Bootstrap
	*/
	class Bootsrap
	{
		
		function __construct()
		{
			$arrParam = array(
						'controller'=>'',
						'action'=>array());

			$arrParam['controller'] = empty($_GET['c']) ? CONTROLLER_DEFAULT : $_GET['c'];
			$arrParam['action'] = empty($_GET['a']) ? ACTION_DEFAULT : $_GET['a'];

			require_once 'libs\Controller.php';
			$controller = new Controller();

			if(($arrParam['controller']=='index')&&($arrParam['action']=='index'))
			{
				require_once 'index.php';
			}	
			else 
			{
				$controller->load($arrParam['controller'], $arrParam['action']);
			}

		}
	}	
		
?>