<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>MVC-OOP</title>
	<link rel="stylesheet" href="templates/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="templates/css/nprogress.css">
    <script src="templates/js/jquery-2.0.3.js"></script>
    <script src="templates/js/nprogress.js"></script>
    <script src="templates/js/load-content.js"></script>
    <script src="templates/js/back-top.js"></script>
   
</head>
<body>
	 <script type="text/javascript">
    	NProgress.done(true);
	</script>
	<div id="container">
		<div id="header">
	        <div id="header-main">
		        <div class = "logo">
		             <h1><a href="#">Continuum</a></h1>
		        </div><!-- end .logo -->
		        
		        <div id = "nav-menu">
		             <ul class="menu">
		                <li class="active"><a href="#">Home</a></li>
		                <li><a href="#">About</a></li>
		                <li><a href="#">Contact</a></li>
		                <li><a href="#">Full Width</a></li>
                		<li><a href="#">ShortCodes</a></li>
		                <li><a href="index.php?c=user&a=register">Register</a></li>
		                <li><a href="index.php?c=user&a=login">Login</a></li> 
		             </ul>
		        </div><!-- end #nav-menu -->
	        </div><!-- end #header-main -->
	    </div><!-- end #header -->

	
