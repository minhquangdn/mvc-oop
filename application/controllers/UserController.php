<?php 
	/**
	* User Controller
	*/
	require_once 'libs/View.php';
	require_once 'application/models/UserModel.php';

	class UserController extends Controller
	{
		
		public $model;
		public function __construct()
		{
			$this->model = new UserModel();
		}

		public function registerAction() {
			
			require_once 'application/views/user/register.php';

			if(empty($_POST))
			{
				echo 'POST rong';
			}
			else
			{
				$register = $this->model->insert_user($_POST['username'], $_POST['password'], $_POST['email'], $_POST['fullname']);
				
				if($register)
				{
					echo 'Registration successfully !';
				}
				else
				{
					echo 'Registration failed ! '.$_POST['username'].' already exist. Please try again';
				}
			}
			
		}
		public function loginAction() {
			echo "Trang login";
		}
	}
?>