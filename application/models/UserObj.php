<?php 
	/**
	* UserObj
	*/
	class UserObj {
		
		protected $userid;
		protected $username;
		protected $password;
		protected $email;
		protected $fullname;

		public function setUserId($userid) {
			$this->userid = $userid;
		}

		public function getUserId() {
			return $this->userid;
		}

		public function setUsername($username) {
			$this->username = $username;
		}

		public function getUsername() {
			return $this->username;
		}

		public function setPassword($password) {
			$this->password = $password;
		}

		public function getPassword() {
			return $this->password;
		}
		public function setEmail($email) {
			$this->email = $email;
		}

		public function getEmail() {
			return $this->email;
		}

		public function setFullname($fullname) {
			$this->fullname = $fullname;
		}

		public function getFullname() {
			return $this->fullname;
		}
	}
?>