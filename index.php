<?php 
	/**
	* Bootstrap
	*/

	require_once 'config/config.php';
	require_once 'libs/Controller.php';
	require_once 'application/controllers/UserController.php';
	require_once 'templates/header.php';
	require_once 'libs/bootstrap.php';

	$bootstrap = new Bootsrap();
		
	require_once 'templates/footer.php';
?>